angular.module('constants', [])

    .constant('questions', function() {
        return QUESTIONS = {
            1: {
                textTu: 'Est-ce qu\'on se tutoie?',
            },
            2: {
                textTu: 'Ton entreprise existe-t\'elle?',
            },
            3: {
                textTu: 'Vends-tu des services?',
            },
            4: {
                textTu: 'Vends-tu des produits?'
            },
            5: {
                textTu: 'Est-ce que tu les vends sur internet?'
            },
            6: {
                textTu: 'Est-ce que tu les vends en magasin?'
            },
            7: {
                textTu: 'As-tu des clients professionnels?',
            },
            8: {
                textTu: 'As-tu des clients particuliers?',
            },
            9: {
                textTu: 'Ton secteur d\'activité est-il lié à l\'agroalimentaire?',
            },
            10: {
                textTu: 'Ton secteur d\'activité est-il lié au bâtiment?',
            },
            11: {
                textTu: 'Ton secteur d\'activité est-il lié au high-tech?',
            },
            12: {
                textTu: 'Développe tu en interne tes produits?',
            },
            13: {
                textTu: 'Fabriques-tu tes produits en interne?',
            },
            14: {
              textTu: "As-tu des salariés?",
            },
            15: {
                textTu: "Es-tu propriétaire de tes locaux?",
            },
            16: {
              textTu: "Es-tu locataire de tes locaux?"
            },
            17: {
              textTu: "Possède-tu un véhicule d'entreprise?"
            },
            18: {
              textTu: "Possède-tu du matériel informatique?"
            }
        }
    })
    .constant('questionsNonBin', function()
    {
        return QUESTIONS = {
            1: {
                textTu: "Comment t'appelles tu ? (nom et prénom)",
                type: "TEXT"
            },
        };
    });