var myApp = angular.module('hackathon', ['ui.router', 'ngAnimate', 'ngTouch', 'constants']);

// myApp.config(function($stateProvider){
//     $stateProvider.state('index', {
//         url: '/menu',
//         abstract: true,
//         templateUrl: 'templates/main.html',
//     })
// });

myApp.controller('MainCtrl', function ($scope, $timeout, questionBin, questionNonBin) {
    $scope.welcomeText = "Vous voulez trouver votre assureur idéal ?";
    $scope.buttonWelcome = "Le rechercher maintenant";
    $scope.questBin = undefined;
    $scope.questionNb = 0;
    $scope.blockAnimation;
    $scope.resultat = undefined;
    $scope.questNonBin = undefined;
    $scope.partTwo = false;
    $scope.displayQuestionsBin = function(nb, tu)
    {
        if ($scope.questBin === undefined)
            $scope.questBin = [];
        $scope.questBin.push(tu !== undefined ? questionBin.get(nb, tu) : questionBin.get(nb));
    };
    $scope.answer = function(a)
    {
        if (a === 'OUI')
            $scope.blockAnimation = ".animBlock{transform: translateX(150%); opacity: 0; transition: all .3s;}";
        else
            $scope.blockAnimation = ".animBlock{transform: translateX(-150%); opacity: 0; transition: all .3s;}";
        $timeout(function(event){
            $scope.blockAnimation = '';
            $scope.questionNb++;
            if ($scope.questionNb - 1 === 0){
                $scope.displayQuestionsBin($scope.questionNb, (a === 'OUI'));
            }
            else{
                $scope.displayQuestionsBin($scope.questionNb);
            }
            console.log("valeur du dernier element = ", $scope.questBin[$scope.questBin.length - 1]);
            if (questionBin.lastQ()){
                $scope.questionNb = 0;
                displayResult();
            }
        }, 300);

    };
    $scope.$watch(function(){
        console.log(questionBin.lastQnb());
        return questionBin.lastQnb()
    },
    function(old, newv) {
        $scope.lastQ = newv + 1;
    })
    var displayResult = function()
    {
        $scope.resultat = {prenom: "Guillaume", img: "guillaume.jpg"};
    };
    $scope.displayPartTwo = function()
    {
        $scope.partTwo = true;
        $scope.partTwoText = $scope.resultat.prenom + " aimerait continuer à en apprendre sur toi avant l'entrevue";
        $scope.partTwoButton1 = "Okay!";
        $scope.partTwoButton2 = "Je n'ai pas le temps...";
    };
    $scope.displayQuestionsNonBin = function(nb)
    {
        if ($scope.questNonBin === undefined)
            $scope.questNonBin = [];
        var tu = questionBin.getTu();
        $scope.questNonBin.push(tu !== undefined ? questionNonBin.get(nb, tu) : questionNonBin.get(nb));
        console.log("fonction récupérée = ", $scope.questNonBin[$scope.questionNb]);
    };
    $scope.displayCalendar = function()
    {

    }
});


myApp.factory('questionBin', function(questions){
    var lastQnb;

    var get = function(nb, t)
    {
        lastQnb = nb;
        console.log("question à récuperer = ", nb);
        return (questions()[nb+1]);
    };

    var lastQ = function()
    {
        return lastQnb == 18;
    };

    return {
        get : get,
        getTu : function()
        {
            return (true);
        },
        lastQ: lastQ,
        lastQnb: function() {return lastQnb}
    };
});

myApp.factory('questionNonBin', function(questionsNonBin){
    var tu = undefined;
    var get = function(nb, t)
    {
        lastQnb = nb;
        console.log("question à récuperer = ", nb);
        return (questionsNonBin()[nb+1]);
    };
    var lastQ = function()
    {
        return lastQnb == lastQnb;
    };
    return {
        get : get,
        lastQ: lastQ
    };
});

// myApp.directive('reponse')
